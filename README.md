# SGFD 2021 Water Rates

A web application demonstrating an alternative metered water rate system with tiered pay scales for the community of 190 households in the South Georgia Fire District of Vermont.

The application is hosted at [https://water-rates.vtkellers.com](https://water-rates.vtkellers.com).

# `systemd` Service

The service file can be used to configure a `systemd` service for easier management on a Linux system.
Edit the `WorkingDirectory` and `ExecStart` fields as appropriate.

E.g.,

```bash
sudo cp systemd/sgfd-website.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable sgfd-website.service
sudo systemctl start sgfd-website.service
```
