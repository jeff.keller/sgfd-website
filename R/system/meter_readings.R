
require(plotly)
require(lubridate)
require(data.table)

get_system_meter_readings <- function(path = file.path("data", "system_meter_readings.csv")) {
  
  system_meter_readings <- fread(path)
  system_meter_readings[, newMeterInstalled := grepl(Notes, pattern = "New meter", ignore.case = TRUE)]
  system_meter_readings[, fixedLeak := grepl(Notes, pattern = "leak", ignore.case = TRUE)]
  system_meter_readings[, Date := as.Date(paste(Year, Month, "01", sep = "-"))]
  return(system_meter_readings[])
  
}

plot_system_meter_readings <- function(title = "System Meter Readings") {
  
  system_meter_readings <- get_system_meter_readings()
  
  plot_ly() %>%
    add_trace(
      name = "Gallons",
      data = system_meter_readings,
      x = ~Date, y = ~Difference,
      type = "scatter", mode = "lines"
    ) %>%
    add_trace(
      name = "",
      data = system_meter_readings[(fixedLeak)],
      x = ~Date, y = ~Difference + 25000,
      type = "scatter", mode = "text", text = "🛠️",
      size = I(15), hovertext = ~Notes, hoverinfo = "x+text",
      showlegend = FALSE
    ) %>%
    layout(
      title = list(
        text = title
      ),
      hovermode = "x unified",
      xaxis = list(
        type = "date",
        hoverformat = "%b %Y",
        rangeselector = list(
          buttons = list(
            list(
              count = 1,
              label = "1 yr",
              step = "year",
              stepmode = "backward"
            ),
            list(
              count = 5,
              label = "5 yr",
              step = "year",
              stepmode = "backward"
            ),
            list(
              count = 1,
              label = "YTD",
              step = "year",
              stepmode = "todate"
            ),
            list(step = "all"))
        )
      ),
      yaxis = list(
        title = "Gallons",
        rangemode = "tozero",
        tickformat = ",",
        fixedrange = TRUE
      )
    ) %>%
    config(
      displaylogo = FALSE,
      modeBarButtonsToRemove = c("select2d", "lasso2d", "hoverClosestCartesian", "hoverCompareCartesian", "toggleSpikelines")
    )
  
}

plot_system_meter_decomposition <- function(title = "System Meter Readings\n(Decomposed)") {
  
  system_meter_readings <- get_system_meter_readings()
  dc <- system_meter_readings[, decompose(ts(Difference, freq = 12, start = c(2002, 11)))]
  date_sequence <- system_meter_readings$Date
  
  p_trend <- plot_ly(
    name = "Trend",
    x = date_sequence, y = dc$trend,
    type = "scatter", mode = "lines",
    height = 800
  ) %>% layout(
    yaxis = list(hoverformat = ",.0f", tickformat = ",", title = "Trend", fixedrange = TRUE, rangemode = "tozero")
  )
  p_seasonal <- plot_ly(
    name = "Seasonal",
    x = date_sequence, y = dc$seasonal,
    type = "scatter", mode = "lines",
    height = 800
  ) %>% layout(
    yaxis = list(hoverformat = ",.0f", tickformat = ",", title = "Seasonal", fixedrange = TRUE)
  )
  p_rand <- plot_ly(
    name = "Random",
    x = date_sequence, y = dc$random,
    type = "scatter", mode = "lines",
    height = 800
  ) %>% layout(
    yaxis = list(hoverformat = ",.0f", tickformat = ",", title = "Random\nFluctuations", fixedrange = TRUE)
  )
  
  subplot(
    p_trend, p_seasonal, p_rand, nrows = 3, shareX = TRUE, titleY = TRUE
  ) %>%
    layout(
      title = list(
        text = title
      ),
      hovermode = "x unified",
      xaxis = list(
        type = "date",
        hoverformat = "%b %Y",
        rangeselector = list(
          buttons = list(
            list(
              count = 1,
              label = "1 yr",
              step = "year",
              stepmode = "backward"
            ),
            list(
              count = 5,
              label = "5 yr",
              step = "year",
              stepmode = "backward"
            ),
            list(
              count = 1,
              label = "YTD",
              step = "year",
              stepmode = "todate"
            ),
            list(step = "all"))
        )
      )
    ) %>%
    config(
      displaylogo = FALSE,
      modeBarButtonsToRemove = c("select2d", "lasso2d", "hoverClosestCartesian", "hoverCompareCartesian", "toggleSpikelines")
    ) %>%
    hide_legend()
  
}

system_readings_data_buttons_UI <- function() {
  
  system_meter_readings <- get_system_meter_readings()
  pdfs <- list.files("www/ops_reports", pattern = "^\\d{4}-\\d{2}.pdf$", full.names = TRUE)
  dt <- data.table(year_month = tools::file_path_sans_ext(basename(pdfs)))
  dt[, year := as.integer(substr(year_month, start = 1, stop = 4))]
  dt[, month := as.integer(substr(year_month, start = 6, stop = 7))]
  
  # Identify missing months
  dt[, missing := FALSE]
  dt[, date := as.Date(paste(year, month, "01", sep = "-"))]
  min_date <- min(system_meter_readings$Date)
  today <- Sys.Date()
  max_date <- if (day(today) >= 25) today else today %m-% months(1)
  valid_dates <- data.table(
    date = seq(from = min_date, to = max_date, by = "1 month"),
    missing = TRUE
  )
  valid_dates[dt, missing := i.missing, on = .(date)]
  valid_dates[, year := year(date)]
  valid_dates[, month := month(date)]
  
  setorder(valid_dates, -year, month)
  
  ui <- list()
  for (yr in unique(valid_dates$year)) {
    
    month_ui_list <- mapply(
      month = valid_dates[year == yr, month], missing = valid_dates[year == yr, missing],
      MoreArgs = list(year = yr),
      FUN = system_readings_month_UI, SIMPLIFY = FALSE
    )
    year_ui_list <- c(list(shiny::h4(yr)), month_ui_list)
    ui <- c(ui, year_ui_list)
    
  }
  
  return(ui)
  
}

system_readings_month_UI <- function(year, month, missing = FALSE) {
  
  ym <- paste(year, stringr::str_pad(month, pad = "0", width = 2), sep = "-")
  
  inputId <- glue::glue("system_meter_reading-{ym}")
  ui <- shiny::actionButton(
    inputId = inputId,
    label = month.abb[month],
    style = "margin-bottom: 5px; font-family: monospace;",
    class = ifelse(isFALSE(missing), "", "btn-danger")
  )
  
  if (isFALSE(missing)) {
    ui <- shiny::a(ui, href = glue::glue("ops_reports/{ym}.pdf"), target = "_blank")
  } else {
    ui <- shiny::tagList(
      ui,
      shinyBS::bsPopover(
        id = inputId, title = "Missing",
        content = "The operations report for this month is not available."
      )
    )
  }
  
  return(ui)
  
}

# Render static elements once (on app start)
system_meter_readings_data_UI <- shiny::wellPanel(system_readings_data_buttons_UI())
system_meter_readings_plot <- plot_system_meter_readings(title = NULL)
system_meter_decomposition_plot <- plot_system_meter_decomposition(title = NULL)

# Shiny Modules --------------------------------------------------------------------------------------------------------

usageUI <- function(id) {
  
  ns <- NS(id)
  
  tabPanel(
    title = "Usage",
    h3("System Meter Readings"),
    p(
      "Every gallon of water that enters the system passes through a single meter which measures the water consumption of the entire system.",
      "This consumption is sent to the state of Vermont every month as part of SGFD's reporting requirements.",
      "Below are the monthly readings dating back to November, 2002."
    ),
    tabsetPanel(
      type = "pills",
      tabPanel(
        title = "Plot",
        br(),
        shinycssloaders::withSpinner(
          plotly::plotlyOutput(ns("meter_readings"))
        ),
        em(
          strong("Note:"),
          "A new system meter was installed at the end of April 2024, enabling more accurate readings thereafter."
        ),
        h3("Decomposition"),
        p(
          "The system meter readings can be split (decomposed) into components describing the overall", strong("trend and seasonal pattern,"),
          "along with the remaining random fluctuations in usage."
        ),
        p(
          "Overall, there has been a steady increase in water usage since 2010, though this began to reverse in 2021 after some large leaks were fixed."
        ),
        p(
          "There is also a clear seasonal pattern.",
          "Water usage is up by about 100,000 gallons per month during the summer months compared to the average month.",
          "Similarly, water usage is down by about 100,000 gallons per month during the winter months."
        ),
        shinycssloaders::withSpinner(
          plotly::plotlyOutput(ns("meter_decomposition"), height = 800)
        )
      ),
      tabPanel(
        title = "Data Sources",
        br(),
        shinycssloaders::withSpinner(
          uiOutput(ns("data_sources"))
        )
      )
    ),
    br()
  )

}

usageServer <- function(id) {
  
  moduleServer(
    id,
    function(input, output, session) {
      
      output$meter_readings <- plotly::renderPlotly({
        system_meter_readings_plot
      })
      
      output$meter_decomposition <- plotly::renderPlotly({
        system_meter_decomposition_plot
      })
      
      output$data_sources <- renderUI(system_meter_readings_data_UI)
      
    }
  )
  
}
